package InputOutput;

import ArgumentsStorage.Argument;
import ArgumentsStorage.ArgumentStorage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyChoiceDeserializer {

    private BufferedReader csvReader;
    private String delimiter = ",";
    private boolean header = true;

    public MyChoiceDeserializer(String pathToCsv, String delimiter) throws FileNotFoundException {
        this(pathToCsv);
        this.delimiter = delimiter;
    }

    public MyChoiceDeserializer(String pathToCsv) throws FileNotFoundException {
        this.csvReader = new BufferedReader(new FileReader(pathToCsv));
    }

    private Argument argumentBuilder(String[] data){

        return new Argument(Integer.parseInt(data[0]), data[1], data[2], data[3], data[4],
                data[5], data[6], data[7], data[16]);
    }

    public ArgumentStorage deserialize() throws IOException {
        ArgumentStorage argumentStorage = new ArgumentStorage();
        String row;
        boolean firstLine = true;
        while ((row = csvReader.readLine()) != null) {
            if (!firstLine){
                argumentStorage.addArgument(this.argumentBuilder(row.split(this.delimiter)));
            }
            if (header && firstLine) {
                firstLine = false;
            }
        }
        return argumentStorage;
    }

    public static void main(String[] args) throws IOException {
        MyChoiceDeserializer deserializer = new MyChoiceDeserializer("/Users/francoisledoyen/Documents/FAC/MASTER/MASTER_2/STAGE_INRA/src/my_choice_to_aspartix/data/raw_args.csv");
        ArgumentStorage argumentStorage = deserializer.deserialize();

        for (Argument a : argumentStorage.arguments){
            System.out.println(a.id+","+a.position+","+a.criterion+','+a.nameTypeSource);
        }

    }

}
