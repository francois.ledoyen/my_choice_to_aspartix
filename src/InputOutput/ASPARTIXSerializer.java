package InputOutput;

import ArgumentsStorage.Argument;
import ArgumentsStorage.ArgumentStorage;
import ArgumentsStorage.ArgumentationGraph;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ASPARTIXSerializer {
    public static void serialize(String fileName, ArgumentationGraph argumentationGraph) throws IOException {
        PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));


        for (Argument argument: argumentationGraph.getArguments()) {
            printWriter.println(toStringArg(argument));
        }

        int attacksCpt = 0;
        for (Argument s: argumentationGraph.getAttacks().keySet()){
            for (Argument t: argumentationGraph.getAttacks().get(s)){
                printWriter.println(toStringAttack(s, t));
                attacksCpt ++;
            }
        }
        printWriter.close();
        System.out.println(argumentationGraph.getArguments().size() + " arguments");
        System.out.println(attacksCpt + " attacks");
    }

    private static String toStringArg(Argument argument){
        return "arg("+argument.id+").";
    }

    private static String toStringAttack(Argument s, Argument t){
        return "att("+s.id+","+t.id+").";
    }
}
