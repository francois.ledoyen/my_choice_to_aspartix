package InputOutput;

import ArgumentsStorage.Argument;
import ArgumentsStorage.ArgumentationGraph;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class ArgumentingSkillSerializer {
    public static String delimiter = ",";


    public static void serialize(String fileName, ArgumentationGraph argumentationGraph) throws IOException {
        PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));

        for (Argument argument: argumentationGraph.getArguments()) {
            printWriter.println(toStringArg(argument));
        }
        printWriter.close();
    }

    public static String getHeader(){
        List<String> words = Arrays.asList("id", "position", "criteria", "sourceType");
        return String.join(delimiter, words);
    }

    private static String toStringArg(Argument a){
        List<String> words = Arrays.asList(a.id.toString(), a.position, a.criterion, a.nameTypeSource);
        return String.join(delimiter, words);
    }
}
