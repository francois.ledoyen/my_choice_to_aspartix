package ArgumentsStorage;

public class Argument {
    public Integer id;
    public String stakeHolder;
    public String alternative;
    public String position;
    public String criterion;
    public String aim;
    public String property;
    public String value;
    public String nameTypeSource;

    public Argument(Integer id, String stakeHolder, String alternative, String position, String criterion,
                    String aim, String property, String value, String nameTypeSource) {
        this.id = id;
        this.stakeHolder = stakeHolder;
        this.alternative = alternative;
        this.position = position;
        this.criterion = criterion;
        this.aim = aim;
        this.property = property;
        this.value = value;
        this.nameTypeSource = nameTypeSource;
    }

     public Argument(Integer id, String position, String criterion, String nameTypeSource){
        this(id, null, null, position, criterion, null, null, null, nameTypeSource);
     }

    @Override
    public String toString() {
        return id.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStakeHolder() {
        return stakeHolder;
    }

    public void setStakeHolder(String stakeHolder) {
        this.stakeHolder = stakeHolder;
    }

    public String getAlternative() {
        return alternative;
    }

    public void setAlternative(String alternative) {
        this.alternative = alternative;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCriterion() {
        return criterion;
    }

    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
