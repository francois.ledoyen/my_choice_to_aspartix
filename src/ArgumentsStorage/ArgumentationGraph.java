package ArgumentsStorage;

import java.util.HashMap;
import java.util.List;

public class ArgumentationGraph
{
    ArgumentStorage argumentStorage;
    HashMap<Argument, List<Argument>> attacks;

    public ArgumentationGraph(ArgumentStorage argumentStorage, HashMap<Argument, List<Argument>> attacks) {
        this.argumentStorage = argumentStorage;
        this.attacks = attacks;
    }

    public ArgumentationGraph(ArgumentStorage argumentStorage) {
        this(argumentStorage, new HashMap<>());
    }

    public List<Argument> getArguments(){
        return this.argumentStorage.arguments;
    }

    public HashMap<Argument, List<Argument>> getAttacks() {
        return attacks;
    }

}
