package ArgumentsStorage;

import AttacksComputing.ComputeAttackCriterion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ArgumentStorage{
    public List<Argument> arguments;

    public ArgumentStorage(List<Argument> arguments) {
        this.arguments = arguments;
    }

    public ArgumentStorage(){
        this.arguments = new ArrayList<>();
    }

    public void addArgument(Argument argument){
        arguments.add(argument);
    }

    public List<Argument> getProArguments(){
        return arguments.stream().filter(arg -> arg.position.equals("+"))
                .collect(Collectors.toList());
    }

    public List<Argument> getConArguments(){
        return  arguments.stream().filter(arg -> arg.position.equals("-"))
                .collect(Collectors.toList());
    }
}
