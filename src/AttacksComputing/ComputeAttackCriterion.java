package AttacksComputing;

import ArgumentsStorage.Argument;
import ArgumentsStorage.ArgumentStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ComputeAttackCriterion implements IStrategyComputeAttack{

    @Override
    public boolean attacks(Argument a1, Argument a2) {
        return a1.criterion.equals(a2.criterion) && !a1.position.equals(a2.position);
    }

    @Override
    public HashMap<Argument, List<Argument>> computeAttacks(ArgumentStorage argumentStorage) {
        HashMap<Argument, List<Argument>> attacks = new HashMap<>();

        ArrayList<Argument> proArgs = (ArrayList<Argument>) argumentStorage.getProArguments();
        ArrayList<Argument> conArgs = (ArrayList<Argument>) argumentStorage.getConArguments();

        for (Argument pa: proArgs) {
            for (Argument ca: conArgs){
                if (this.attacks(pa, ca)){
                    attacks.putIfAbsent(pa, new ArrayList<>());
                    attacks.putIfAbsent(ca, new ArrayList<>());

                    attacks.get(pa).add(ca);
                    attacks.get(ca).add(pa);
                }
            }
        }
        return attacks;
    }
}
