package AttacksComputing;

import ArgumentsStorage.Argument;
import ArgumentsStorage.ArgumentStorage;

import java.util.HashMap;
import java.util.List;

public interface IStrategyComputeAttack {
    public boolean attacks (Argument a1, Argument a2);

    public HashMap<Argument, List<Argument>> computeAttacks(ArgumentStorage argumentStorage);
}
