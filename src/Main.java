import ArgumentsStorage.ArgumentStorage;
import ArgumentsStorage.ArgumentationGraph;
import AttacksComputing.ComputeAttackCriterion;
import AttacksComputing.IStrategyComputeAttack;
import InputOutput.ASPARTIXSerializer;
import InputOutput.ArgumentingSkillSerializer;
import InputOutput.MyChoiceDeserializer;

public class Main {

    public static void main(String[] args) throws Exception {
        String inputFilePath = args[0];
        String outputFilePathArgumentationGraph = args[1];
        String outputFilePathArgumentsDescription = args[2];

        IStrategyComputeAttack attackComputer = new ComputeAttackCriterion();
        ArgumentStorage argumentStorage = new MyChoiceDeserializer(inputFilePath).deserialize();
        ArgumentationGraph argumentationGraph = new ArgumentationGraph(argumentStorage, attackComputer.computeAttacks(argumentStorage));

        ASPARTIXSerializer.serialize(outputFilePathArgumentationGraph, argumentationGraph);
        ArgumentingSkillSerializer.serialize(outputFilePathArgumentsDescription, argumentationGraph);
    }
}